"""
PI planning server module
-------------

Gathered common functionality and data to use in pi microservices.
"""
import codecs
import os.path

from setuptools import find_packages
from setuptools import setup


def read(rel_path):
    here = os.path.abspath(os.path.dirname(__file__))
    with codecs.open(os.path.join(here, rel_path), "r") as fp:
        return fp.read()


def get_version(rel_path):
    for line in read(rel_path).splitlines():
        if line.startswith("__version__"):
            delim = '"' if '"' in line else "'"
            return line.split(delim)[1]
    else:
        raise RuntimeError("Unable to find version string.")


setup(
    name="rentouch_shared",
    version=get_version("rentouch_shared/__init__.py"),
    url="https://gitlab.com/rentouch-public/rentouch-shared",
    author="Rentouch",
    author_email="dominique.burnand@rentouch.ch",
    description="Common functionality and data to use in pi micro-services",
    packages=find_packages(),
    long_description=read("README.md"),
    python_requires=">=3.6.5",
    install_requires=[
        "autobahn>=21.3.1",
        "redis>=5.0.1",
        "sentry-sdk>=1.4.3",
        "pyJWT>=2.4.0",
    ],
    classifiers=[
        "Topic :: Utilities",
    ],
)
