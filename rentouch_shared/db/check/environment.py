from typing import _BaseGenericAlias
from typing import _LiteralGenericAlias
from typing import _UnionGenericAlias

from rentouch_shared.db.check import type_of
from rentouch_shared.db.check.checker import Checker
from rentouch_shared.db.check.patcher import Patcher


class Environment:
    def __init__(self):
        self.patcher = Patcher()
        self._patch()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, traceback):
        self.patcher.reset()

    def checker(self, model):
        return Checker(model, self.patcher)

    def _patch(self):
        def union_instancecheck(self, obj):
            for arg in self.__args__:
                if isinstance(obj, type_of(arg)):
                    return True

        def base_instancecheck(self, obj):
            if self._name == "List" and hasattr(self, "__args__"):
                return isinstance(obj, list) and all(
                    isinstance(a, self.__args__[0]) for a in obj
                )
            return self.__subclasscheck__(type(obj))

        def literal_instancecheck(self, obj):
            for arg in self.__args__:
                if arg == obj:
                    return True

        self.patcher.patch(_UnionGenericAlias, "__instancecheck__", union_instancecheck)
        self.patcher.patch(_BaseGenericAlias, "__instancecheck__", base_instancecheck)
        self.patcher.patch(
            _LiteralGenericAlias, "__instancecheck__", literal_instancecheck
        )
