import inspect
import sys
from typing import _LiteralGenericAlias
from typing import _TypedDictMeta

from rentouch_shared.db.check import data_line
from rentouch_shared.db.check import indent_join
from rentouch_shared.db.check.mapping import Mapping


class Checkers:
    def __init__(self):
        self.checkers = {}

    def add(self, db, checker):
        self.checkers[db] = checker
        return checker

    def __repr__(self):
        return indent_join(
            [f"{key}:{value.long_repr(2)}" for key, value in self.checkers.items()], 0
        )


class Checker:
    def __init__(self, model, patcher):
        self.refs = {}
        self.mappings = []
        self.unmapped_collections = []
        self.unmapped_models = []
        self.problems = []
        self.models = inspect.getmembers(
            sys.modules[model.__module__],
            lambda member: inspect.isclass(member)
            and member.__module__ == model.__module__,
        )
        self.parents = self._find_parents()
        patcher.patch(_TypedDictMeta, "__instancecheck__", self.typed_instancecheck())

    def _find_parents(self):
        parents = {}
        for name, model in self.models:
            parent = next(
                (
                    parent
                    for _, parent in self.models
                    if getattr(model, "__parent__", None) == parent
                ),
                None,
            )
            if parent:
                discriminants = [
                    (prop, value)
                    for prop, value in model.__annotations__.items()
                    if isinstance(value, _LiteralGenericAlias)
                    and len(value.__args__) == 1
                ]
                if len(discriminants) != 1:
                    self.problems.append(
                        f"{model} inherits from {parent} so it must have exactly one literal type with one option"
                    )
                else:
                    disc_prop, disc_type = discriminants[0]
                    existing = parents.get(parent)
                    if existing:
                        if existing["discriminant"] != disc_prop:
                            self.problems.append(
                                f"Different discriminants for base type {parent}: "
                                f"{existing['discriminant']} and {disc_prop}"
                            )
                    else:
                        existing = parents[parent] = {
                            "discriminant": disc_prop,
                            "subclasses": {},
                        }
                    existing["subclasses"][disc_type.__args__[0]] = model
        return parents

    def long_repr(self, indent):
        problems = data_line(self.problems, "problems: %s", indent)
        uc = data_line(self.unmapped_collections, "unmapped collections: %s", indent)
        um = data_line(
            [m.__name__ for m in self.unmapped_models], "unmapped models: %s", indent
        )
        mappings = indent_join(
            [m.long_repr(indent) for m in sorted(self.mappings)], indent
        )
        return problems + uc + um + mappings

    def typed_instancecheck(self):
        def check(typed_self, obj):
            mapping = self.mapping_of_model(typed_self)
            self.check_entry(mapping, obj)
            return True

        return check

    def find_model(self, collection):
        parts = collection.split("_")
        model_name = parts[0] + "".join(part.title() for part in parts[1:])
        return next(
            (
                model
                for (name, model) in self.models
                if name.lower() == model_name.lower()
            ),
            None,
        )

    def mapping_of_model(self, model, create=True):
        mapping = next(
            (mapping for mapping in self.mappings if mapping.model == model),
            None,
        )
        if create and not mapping:
            if model in self.unmapped_models:
                self.unmapped_models.remove(model)
            mapping = Mapping(model)
            self.mappings.append(mapping)
        return mapping

    def mapping_of_collection(self, collection):
        return next(
            (mapping for mapping in self.mappings if mapping.collection == collection),
            None,
        )

    def check_collections(self, collections):
        for collection in collections:
            model = self.find_model(collection)
            if model:
                self.mappings.append(Mapping(model, collection))
            else:
                self.unmapped_collections.append(collection)

        for (name, model) in self.models:
            mapping = self.mapping_of_model(model, create=False)
            if not mapping:
                self.unmapped_models.append(model)

        id_props = {}
        for mapping in self.mappings:
            if mapping.id_key and mapping.collection:
                id_type = mapping.model_props[mapping.id_key]["def"]
                if id_type in id_props:
                    self.problems.append(
                        f"Using the same id type in {mapping.model.__name__} and {id_props['id_type'].model.__name__}"
                    )
                id_props[id_type] = mapping
        for mapping in self.mappings:
            for prop, model_prop in mapping.model_props.items():
                if target := id_props.get(model_prop["def"]):
                    if target != mapping:
                        model_prop["ref"] = target

    def check_entry(self, mapping, entry):
        key = entry.get(mapping.model_key)
        if parent := self.parents.get(mapping.model):
            disc_value = entry.get(parent["discriminant"])
            model = parent["subclasses"].get(disc_value)
            if model:
                mapping = self.mapping_of_model(model)
            else:
                mapping.add_unspecialized(disc_value, key)
        for name, value in entry.items():
            model_prop = mapping.model_props.get(name)
            if model_prop:
                if not isinstance(value, model_prop["type"]):
                    mapping.add_wrong_value(name, value, key)
                if ref := model_prop.get("ref"):
                    ref_col = self.refs.setdefault(ref.collection, {})
                    ref_col.setdefault(value, []).append({"prop": name, "key": key})
            else:
                mapping.add_unmapped_prop(name, value, key)
        for name, prop in mapping.model_props.items():
            if name not in entry and not isinstance(None, prop["type"]):
                mapping.add_missing_prop(name, key)

    def add_missing_ref(self, col, value, sources):
        mapping = self.mapping_of_collection(col)
        for source in sources:
            mapping.add_missing_ref(source["prop"], str(value), source["key"])
