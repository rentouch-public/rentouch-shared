from twisted.internet import ssl
from txmongo import Query
from txmongo.connection import ConnectionPool


class Connection:
    def __init__(self, conn, proto):
        self.conn = conn
        self.proto = proto

    @staticmethod
    async def create(user, pw, url, tls):
        url = f"mongodb://{user}:{pw}@{url}/admin"
        conn = ConnectionPool(
            url, ssl_context_factory=ssl.ClientContextFactory() if tls else None
        )
        proto = await conn.getprotocol()
        return Connection(conn, proto)

    async def databases(self):
        query = Query(collection="admin.$cmd", query={"listDatabases": 1})
        reply = await self.proto.send_QUERY(query)
        return [db["name"] for db in reply.documents[0].decode()["databases"]]

    async def collections(self, database):
        query = Query(collection=f"{database}.$cmd", query={"listCollections": 1})
        reply = await self.proto.send_QUERY(query)
        return [
            col["name"] for col in reply.documents[0].decode()["cursor"]["firstBatch"]
        ]

    async def count(self, database, collection):
        return await self.conn[database][collection].count()

    async def get_by_id(self, database, collection, _id):
        return await self.conn[database][collection].find_one({"_id": _id}, {"_id": 1})

    async def do_with_entries(self, database, collection, action, **kwargs):
        count = 0
        entries, cursor = await self.conn[database][collection].find_with_cursor(
            batch_size=100
        )
        while len(entries) > 0 and count < kwargs.get("max_entries", 1000):
            print(".", end="")
            count += len(entries)
            for entry in entries:
                action(entry)
            entries, cursor = await cursor
