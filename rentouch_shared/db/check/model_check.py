from rentouch_shared.db.check.checker import Checkers
from rentouch_shared.db.check.environment import Environment


async def check_model(connection, typed_dict, **kwargs):
    with Environment() as env:
        dbs = await connection.databases()
        checkers = Checkers()
        for db in dbs:
            if kwargs.get("db_filter", no_filter)(db):
                print(f"Checking db {db}")
                cols = await connection.collections(db)
                checker = checkers.add(db, env.checker(typed_dict))
                checker.check_collections(cols)
                for col in cols:
                    if kwargs.get("col_filter", no_filter)(col):
                        count = await connection.count(db, col)
                        print(f"Checking collection {col}, {count} entries", end="")
                        if mapping := checker.mapping_of_collection(col):
                            await connection.do_with_entries(
                                db,
                                col,
                                lambda entry: checker.check_entry(mapping, entry),
                                **kwargs,
                            )
                            ref_count = sum(len(refs) for refs in checker.refs.values())
                            print(f"{ref_count} refs", end="")
                            for ref_col, refs in checker.refs.items():
                                for _id, sources in refs.items():
                                    target = await connection.get_by_id(
                                        db, ref_col, _id
                                    )
                                    if not target:
                                        checker.add_missing_ref(col, _id, sources)
                            checker.refs.clear()
                        print(".")
        return checkers


def no_filter(_):
    return True
