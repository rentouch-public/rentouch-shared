def line(line, indent):
    return "\n" + "  " * indent + line


def indent_join(values, indent):
    if len(values) == 0:
        return ""
    return line("", indent) + line("", indent).join(
        [item for item in values if len(item) > 0]
    )


def dict_repr(mapping, indent):
    return (
        indent_join([f"{key}: {value}" for key, value in mapping.items()], indent)
        if len(mapping) > 0
        else ""
    )


def data_line(data, s, indent):
    return line(s % data, indent) if len(data) > 0 else ""


def specializes(base):
    def wrapper(f):
        f.__parent__ = base
        return f

    return wrapper


def type_of(value):
    if value.__class__.__name__ == "NewType" or getattr(
        value, "__qualname__", ""
    ).startswith("NewType."):
        return value.__supertype__
    return value
