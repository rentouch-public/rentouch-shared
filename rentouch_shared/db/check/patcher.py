class Patcher:
    def __init__(self):
        self.patches = {}

    def patch(self, clazz, method, replacement):
        self.patches.setdefault(clazz, {})[method] = getattr(clazz, method)
        setattr(clazz, method, replacement)

    def reset(self):
        for clazz, methods in self.patches.items():
            for method, func in methods.items():
                setattr(clazz, method, func)
