from rentouch_shared.db.check import dict_repr
from rentouch_shared.db.check import line
from rentouch_shared.db.check import type_of


class Mapping:
    def __init__(self, model, collection=None):
        self.model = model
        self.collection = collection or ""
        self.unmapped_props = {}
        self.missing_props = {}
        self.unspecialized = {}
        self.missing_refs = {}
        self.wrong_value = {}
        self.model_props = {
            prop: {"def": value, "type": type_of(value)}
            for prop, value in model.__annotations__.items()
        }
        self.model_key = (
            list(self.model_props.keys())[0] if len(self.model_props) > 0 else "empty"
        )
        id_prop = self.model_props.get("_id")
        self.id_key = "_id" if id_prop and id_prop["def"] != id_prop["type"] else None

    def add_unmapped_prop(self, prop, value, _id):
        self.unmapped_props.setdefault(prop, {})[_id] = value

    def add_wrong_value(self, prop, value, _id):
        self.wrong_value.setdefault(prop, {})[_id] = value

    def add_missing_prop(self, prop, _id):
        self.missing_props.setdefault(prop, []).append(_id)

    def add_unspecialized(self, value, _id):
        self.unspecialized.setdefault(value, []).append(_id)

    def add_missing_ref(self, prop, value, _id):
        self.missing_refs.setdefault(prop, {})[_id] = value

    def __repr__(self):
        return f"{self.collection}->{self.model.__name__}"

    def long_repr(self, indent):
        up = dict_repr(self.unmapped_props, indent + 2)
        mp = dict_repr(self.missing_props, indent + 2)
        us = dict_repr(self.unspecialized, indent + 2)
        mr = dict_repr(self.missing_refs, indent + 2)
        wv = dict_repr(self.wrong_value, indent + 2)
        name = (
            line(f"{self.model.__name__}:", indent)
            if len(up) + len(mp) + len(us) + len(mr) + len(wv) > 0
            else ""
        )
        return (
            name
            + (
                line(f"unmapped props:{up}", indent + 1)
                if len(self.unmapped_props) > 0
                else ""
            )
            + (
                line(f"missing props:{mp}", indent + 1)
                if len(self.missing_props) > 0
                else ""
            )
            + (
                line(f"unspecialized parent:{us}", indent + 1)
                if len(self.unspecialized) > 0
                else ""
            )
            + (
                line(f"missing refs:{mr}", indent + 1)
                if len(self.missing_refs) > 0
                else ""
            )
            + (
                line(f"wrong value:{wv}", indent + 1)
                if len(self.wrong_value) > 0
                else ""
            )
        )

    def __lt__(self, other):
        if self.collection != other.collection:
            return False if not self.collection else self.collection < other.collection
        return self.model.__name__ < other.model.__name__
