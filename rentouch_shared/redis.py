from typing import Protocol


class Redis(Protocol):
    async def get(self, key: str) -> any:
        ...

    async def set(self, key: str, value: any) -> None:
        ...

    async def expire(self, key: str, ttl: int) -> None:
        ...
