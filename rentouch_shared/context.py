from __future__ import annotations

from collections.abc import Awaitable
from collections.abc import Callable
from contextvars import ContextVar
from typing import Literal
from typing import Self
from typing import TypedDict
from uuid import uuid4

from sentry_sdk import Hub
from sentry_sdk import Scope
from sentry_sdk.hub import GLOBAL_HUB


class ContextSession:
    async def subscribe_message(self, uri, handler, options=None):
        print(f"WARNING: Actual subscribe not implemented to {uri}")
        return await _resolve(f"Published {uri}")

    def publish_message(self, uri, *args, **kwargs):
        print(f"WARNING: Actual publish not implemented to {uri}")
        return f"Published {uri}"

    async def call_procedure(self, uri, *args, **kwargs):
        print(f"WARNING: Actual rpc not implemented to {uri}")
        return await _resolve(f"Called {uri}")


async def _resolve(value):
    return value


Entity = Literal["train", "art", "team"]


class Entities(TypedDict):
    train: list[int]
    art: list[int]
    team: list[int]


Permission = Literal["read", "edit", "session"]


class Permissions(TypedDict):
    read: Entities
    edit: Entities
    session: Entities


class ExecutionContext:
    fetch_permissions: Callable[[Self], Awaitable[Permissions]] | None = None

    def __init__(
        self,
        company: str,
        user_id: str,
        correlation_id: str | None,
        session: ContextSession,
        creation_error: Exception | None = None,
    ):
        self.company = company
        self.user_id = user_id
        self.correlation_id = correlation_id or "X" + str(uuid4())[1:]
        self._session = session
        self.creation_error = creation_error
        self._permissions: Permissions | None = None

    def __enter__(self):
        _current.set(self)
        scope = Scope()
        scope.set_user(self._as_sentry_user())
        self.hub = Hub(GLOBAL_HUB, scope=scope).__enter__()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.hub.__exit__(exc_type, exc_value, traceback)
        _current.set(_empty)

    @staticmethod
    def current():
        return _current.get()

    def _as_sentry_user(self):
        return {
            "id": self.company,
            "user-id": self.user_id,
            "company": self.company,
            "correlation-id": self.correlation_id,
        }

    def with_company(self, company) -> ExecutionContext:
        return ExecutionContext(
            company, self.user_id, self.correlation_id, self._session
        )

    def publish_message(self, uri, *args, **kwargs):
        return self._session.publish_message(
            uri, *args, **kwargs, **{"correlation-id": self.correlation_id}
        )

    async def call_procedure(self, uri, *args, **kwargs):
        if not kwargs.pop("no_correlation_id", None):
            kwargs["correlation-id"] = self.correlation_id
        return await self._session.call_procedure(uri, *args, **kwargs)

    def raise_creation_error(self):
        if self.creation_error:
            raise self.creation_error

    async def get_permissions(self) -> Permissions:
        if not self._permissions:
            if not ExecutionContext.fetch_permissions:
                raise Exception(
                    "Need to set ExecutionContext.fetch_permissions before calling get_permissions"
                )
            self._permissions = await ExecutionContext.fetch_permissions(self)
        return self._permissions


_empty = ExecutionContext("", "", None, ContextSession())
_current = ContextVar("current_context", default=_empty)
