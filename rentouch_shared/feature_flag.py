from collections.abc import Callable
from typing import Protocol

from UnleashClient import UnleashClient


class FeatureFlagService(Protocol):
    def is_enabled(self, feature: str) -> bool:
        """Return if given feature flag is enabled."""
        ...


class DisabledFeatureFlagService(FeatureFlagService):
    def is_enabled(self, feature: str) -> bool:
        return False


class UnleashFeatureFlagService(FeatureFlagService):
    def __init__(self, client: UnleashClient, get_company: Callable[[], str]):
        self.client = client
        self.get_company = get_company

    def is_enabled(self, feature: str) -> bool:
        return self.client.is_enabled(feature, {"userId": self.get_company()})


def create_feature_flag_service(
    get_company: Callable[[], str],
    proxy_url: str,
    instance_id: str,
    environment: str,
    refresh_interval: int = 15,
) -> FeatureFlagService:
    if not proxy_url or not instance_id:
        # log.debug(
        #     "Skip initializing Unleash client as proxy_url or instance_id is missing (which is fine for onprem)"
        # )
        return DisabledFeatureFlagService()
    try:
        client = UnleashClient(
            url=proxy_url,
            app_name=environment,
            instance_id=instance_id,
            refresh_interval=refresh_interval,
            disable_metrics=True,
            disable_registration=True,
        )
        client.initialize_client()
        return UnleashFeatureFlagService(client, get_company)
    except Exception:
        # log.error(f"Error initializing Unleash client: {e}")
        return DisabledFeatureFlagService()
