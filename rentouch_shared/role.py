from enum import StrEnum
from typing import Self


class Role(StrEnum):
    """Enum representing list of roles available in the system."""

    ANONYMOUS = "anonymous"
    OBSERVER = "observer"
    USER = "user"
    PLANNING_INTERVAL_ADMIN = "planning_interval_admin"
    ADMIN = "admin"

    @classmethod
    def from_str(cls, role: str) -> Self:
        if role.startswith("cb_"):
            role = role[3:]
        return Role(role)

    @classmethod
    def user_roles(cls):
        return [
            cls.ADMIN,
            cls.PLANNING_INTERVAL_ADMIN,
            cls.USER,
            cls.OBSERVER,
        ]
