import datetime
import json
from json import JSONEncoder

from bson import ObjectId


class BSONEncoder(JSONEncoder):
    """Use this to convert BSON ObjectID's to strings"""

    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        if isinstance(o, datetime.datetime):
            return str(o)
        return super().default(o)


def decode_bson(bson_dict):
    """Converts a python dict which contains BSON objects to a python dict
    which is JSON compatible.
    E.g. It will convert all ObjectID's to strings.

    :param bson_dict: dictionary containing BSON objects
    :return: dict
    """
    return json.loads(BSONEncoder().encode(bson_dict))
