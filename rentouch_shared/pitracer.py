"""
Client module to interact with pitracer.
This will help you to send the log-entries to the redis queue.
"""
import inspect
import json
import logging
import time

from rentouch_shared.context import ExecutionContext
from rentouch_shared.services.rqueue import RedisQueue


def _get_caller_info() -> tuple[str, int]:
    try:
        frame = inspect.stack()[1]
    except IndexError:
        return "unknown", 0

    lineno = frame.lineno
    try:
        filename = frame.filename.split("/")[-1]
    except IndexError:
        filename = frame.filename

    return filename, lineno


class Tracer:
    def __init__(
        self,
        queue: RedisQueue,
        service_name: str,
        logger: logging.Logger = logging.Logger("pitracer"),
    ):
        self.queue = queue
        self.service_name = service_name
        self.logger = logger

    async def log(
        self,
        triggered_by: str,
        action: str,
        data: dict | None = None,
        log: bool = True,
        context: ExecutionContext | None = None,
        **kwargs,
    ):
        """Pushes a new log entry to the queue. You can pass as many additional
        keyword-arguments as you like. These arguments will then be passed
        to the queue as well. All data needs to be json serializable.

        If self.log is set to a logger, it will also log that event.

        :param triggered_by: by what service or whim got this triggered?
                             (required)
        :param action: what happens? (e.g. sticky_delete) (required)
        :param data: additional data (e.g. {'foo': 'bar'})
        :param log: default to True. If set to False, it will not log to local
                    logger (only to pitracer)
        :param context: ExecutionContext you receive at each wamp-call. This will be used
                             to set 'company' and 'authid'.
        """
        context = context or ExecutionContext.current()
        company = context.company
        user_id = context.user_id

        _log = {
            "timestamp": time.time(),
            "service": self.service_name,
            "company": company,
            "triggered_by": triggered_by,
            "action": action,
            "data": data,
            "authid": f"{company}.{user_id}",
            **kwargs,
        }

        filename, lineno = _get_caller_info()

        if log:
            self.logger.debug(
                f"{filename}:{lineno} [{company}] {action} "
                f'by "{triggered_by}"/{user_id}: '
                f"{kwargs}; data={data}"
            )

        await self.queue.put("logs", json.dumps(_log))
