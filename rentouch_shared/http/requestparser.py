import typing

ReadOnlyCookies = typing.Mapping[str, str]


def access_token_of(cookies: ReadOnlyCookies) -> str | None:
    return cookies.get("pip_at")


def client_id_of(cookies: ReadOnlyCookies) -> str | None:
    return cookies.get("pip_clientId")


def refresh_token_of(cookies: ReadOnlyCookies) -> str | None:
    return cookies.get("pip_rt")
