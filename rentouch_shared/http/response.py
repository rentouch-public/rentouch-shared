import json
from typing import Any


def success_response(data: Any):
    return {"success": True, "response": data}


def fail_response(code: str, message: str, details: str | None = None):
    return {
        "success": False,
        "error": {"code": code, "message": message, "details": details},
    }


NO_AUTH_RESPONSE = (401, fail_response("NO_AUTH", "Login again."))
TOKEN_REVOKED_RESPONSE = (401, fail_response("TOKEN_REVOKED", "Login again."))
TOKEN_EXPIRED_RESPONSE = (
    401,
    fail_response("TOKEN_EXPIRED", "Refresh the token or login again."),
)
TOKEN_REFRESH_ERROR_RESPONSE = (
    401,
    fail_response("TOKEN_REFRESH_ERROR", "Login again."),
)
SERVER_ERROR_RESPONSE = (500, fail_response("SERVER_ERROR", "Something went wrong."))


# same logic as in pi-bridge
def response_of_data(response: Any):
    if isinstance(response, Exception):
        return fail_response(response.__class__.__name__, str(response))

    if not isinstance(response, dict):
        return success_response(response)

    response_keys = sorted(list(response.keys()))
    if response_keys == ["response", "success"]:
        return response
    if "success" in response_keys:
        if not response["success"]:
            code = str(response.get("errorCode", "UNKNOWN_ERROR"))
            message = response.get("error", "An unexpected error occurred")
            return fail_response(code, message)
        if len(response_keys) == 2:
            other_key = (
                response_keys[1] if response_keys[0] == "success" else response_keys[0]
            )
            return success_response(response[other_key])
    return success_response(response)


ResponseCode = int
Headers = dict[str, str]
JsonResponseData = str


def set_response(
    response: dict | tuple, code: int | None = None
) -> tuple[ResponseCode, Headers, JsonResponseData]:
    if isinstance(response, dict):
        code = code or (200 if response.get("success", False) else 500)
        data = response
    else:
        code = code or response[0]
        data = response[1]

    return (
        code,
        {"Cache-Control": "no-cache", "Content-Type": "application/json"},
        json.dumps(data),
    )
