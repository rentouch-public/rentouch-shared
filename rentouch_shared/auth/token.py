import logging

from rentouch_shared.role import Role

log = logging.getLogger("authServer")


class Jwt:
    def __init__(self, data):
        self.data = data

    @property
    def user(self):
        return self.data["sub"]

    @property
    def tenant_id(self):
        return self.data["tenantId"]

    @property
    def roles(self):
        return self.data.get("roles", [])

    def get_role(self, default: Role) -> Role:
        return Role.from_str(self.roles[0]) if self.roles else default


class AppToken(Jwt):
    def __init__(self, data):
        super().__init__(data)

    @property
    def tenant_name(self):
        return self.data["tenantName"]

    @property
    def company_id(self):
        return self.data.get("company_id")

    @property
    def authid(self):
        return f"{self.company_id}.{self.user}"

    def set_roles(self, roles):
        self.data["roles"] = roles

    def __eq__(self, other):
        return isinstance(other, AppToken) and other.data == self.data
