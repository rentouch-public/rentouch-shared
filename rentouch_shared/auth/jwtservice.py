from time import time

import jwt

from rentouch_shared.auth.token import AppToken
from rentouch_shared.auth.token import Jwt
from rentouch_shared.context import ContextSession
from rentouch_shared.redis import Redis


class CookiesNotFound(Exception):
    pass


class RevokedTokenError(Exception):
    pass


REVOKE_ENDPOINT = "ch.rentouch.auth.3.admin.revoke"


class JwtService:
    def __init__(self, pub_key: str, redis: Redis, session: ContextSession):
        self.pub_key = pub_key
        self.redis = redis
        self.session = session

    async def register_revoke(self):
        await self.session.subscribe_message(REVOKE_ENDPOINT, self._revoked)

    async def token_from_cookies(
        self,
        access_token: str | None,
        client_id: str | None,
        pub_key=None,
        token_class=AppToken,
    ) -> Jwt:
        """Decodes an authserver signed jwt token. This function will raise if
        token is invalid.

        :param access_token: the jwt singed by authserver
        :param client_id: application / client-id
        """
        if not access_token or not client_id:
            raise CookiesNotFound()

        payload = jwt.decode(
            access_token,
            pub_key or self.pub_key,
            audience=client_id,
            algorithms=["RS256"],
        )
        revoked = await self._is_revoked(payload)
        if revoked:
            raise RevokedTokenError(f"Access token revoked for user {payload['sub']}")
        return token_class(payload)

    async def _revoked(self, user_id: str, ttl: int, **kwargs):
        await self._redis_set_revoke(user_id, ttl)

    async def _is_revoked(self, jwt_obj: dict):
        revoke = await self._redis_get_revoke(jwt_obj["sub"])
        return revoke and int(revoke) >= jwt_obj["exp"]

    async def _redis_set_revoke(self, user_id: str, ttl: int):
        key = self._redis_key(user_id)
        await self.redis.set(key, int(time()) + ttl)
        await self.redis.expire(key, ttl)

    async def _redis_get_revoke(self, user_id: str):
        key = self._redis_key(user_id)
        return await self.redis.get(key)

    def _redis_key(self, user_id):
        return f"jwtrevoke:{user_id}"
