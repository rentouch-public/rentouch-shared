from autobahn.wamp.message import Invocation

from rentouch_shared.auth.token import Jwt
from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext


def context_of_token(
    token: Jwt, correlation_id, session: ContextSession
) -> ExecutionContext:
    return ExecutionContext(
        token.company_id,
        token.user,
        correlation_id,
        session,
    )


def context_of_message(msg: Invocation, session: ContextSession) -> ExecutionContext:
    authid = msg.caller_authid
    # Call from an actual user / individual
    # authid is always <company_id>.<user-id>
    if "." in authid:  # call from frontend
        company, user_id = authid.split(".", maxsplit=1)
    # Call from one of the backends
    elif msg.procedure:
        company = _find_company_in_uri(msg.procedure)
        user_id = authid
    # Call from cloudadmin interface
    elif msg.caller_authrole == "cloudadmin":
        company = "<unknown>"
        user_id = authid
    # WAMP auth call, e.g session leave call of crossbar
    else:
        try:
            session_arg = msg.args[0]
            company, user_id = session_arg["authid"].split(".", maxsplit=1)
        except Exception:
            company = "<unknown>"
            user_id = "<unknown>"

    if user_id == "backend" and (as_user := msg.kwargs.pop("as_user", None)):
        user_id = as_user

    correlation_id = msg.kwargs.pop("correlation-id", None)
    return ExecutionContext(company, user_id, correlation_id, session)


def context_of_kwargs(kwargs) -> ExecutionContext:
    if context := kwargs.get("context"):
        return context
    raise Exception("Context not found in kwargs")


def context_of_error(
    error: Exception, correlation_id: str, session: ContextSession
) -> ExecutionContext:
    return ExecutionContext(
        "<unknown>",
        "<unknown>",
        correlation_id,
        session,
        creation_error=error,
    )


def _find_company_in_uri(uri):
    start = uri.index(".room.") + 6
    return uri[start : uri.index(".", start + 1)]
