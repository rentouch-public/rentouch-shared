import logging
import os
import sys
from logging.handlers import RotatingFileHandler

from rentouch_shared.context import ExecutionContext


class ContextFilter(logging.Filter):
    def filter(self, record):
        context: ExecutionContext = ExecutionContext.current()
        record.company = context.company
        record.user = context.user_id
        record.correlation_id = context.correlation_id
        return True


def init_logger(version, logger, formatter_class=logging.Formatter, log_dir=None):
    # Fix UTF8 logging in docker containers
    # Taken from: https://stackoverflow.com/a/52372390
    sys.stdout.reconfigure(encoding="utf-8")

    class CustomLogger(logging.Logger):
        def _log(self, level, msg, args, exc_info=None, extra=None):
            if not extra:
                extra = {}
            extra["version"] = version
            super()._log(level, msg, args, exc_info, extra)

    logging.setLoggerClass(CustomLogger)
    log = logging.getLogger(logger)
    log.setLevel(logging.DEBUG)

    # Disable propagation of log messages to root logger
    # if not false, it causes double log lines
    log.propagate = False

    when = "%(asctime)s"
    context = "[%(levelname)-8s %(correlation_id)s]"
    who = "%(user)s.%(company)-20.20s"
    where = "%(module)15.15s:%(lineno)3s"
    what = "%(message)s"
    log_format = f"{when} {context} {who} {where}: {what}"
    formatter = formatter_class(log_format)

    def add_handler(handler):
        handler.addFilter(ContextFilter())
        handler.setFormatter(formatter)
        log.addHandler(handler)
        return handler

    console_handler = add_handler(logging.StreamHandler(sys.stdout))

    loga = logging.getLogger("asyncio")
    loga.addHandler(console_handler)

    # If a user has set a path to a log directory, create a file-handler
    if log_dir:
        try:
            if not os.path.isdir(log_dir):
                os.mkdir(log_dir)
        except Exception as e:
            log.error(f"Was not able to create log directory {log_dir}, {e}")

        add_handler(
            RotatingFileHandler(
                os.path.join(log_dir, f"{logger}.log"), maxBytes=4000000, backupCount=5
            )
        )

        log_file = os.path.join(log_dir, f"{logger}.log")
        logging.basicConfig(filename=log_file, filemode="w", encoding="utf-8")

    return log
