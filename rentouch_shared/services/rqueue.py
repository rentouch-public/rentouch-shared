import logging
import traceback

import redis.exceptions as redis_exceptions
from redis.asyncio import Redis


class RedisQueue:
    """Simple Queue with Redis Backend."""

    def __init__(self, rc: Redis, namespace="queue"):
        self.rc = rc
        self.namespace = namespace

    async def qsize(self, key):
        """Return the approximate size of the queue."""
        ret = await self.rc.llen(key)
        return ret

    async def get(self, key, block=True, timeout=0):
        """Remove and return an item from the queue.

        If optional args block is true and timeout is None (the
        default), block if necessary until an item is available.
        """
        key = f"{self.namespace}:{key}"
        if block:
            item = await self.rc.blpop(key, timeout=timeout)
        else:
            item = await self.rc.lpop(key)

        if item:
            item = item[1]
        return item

    async def put(self, key, item):
        """Put item into the queue."""
        key = f"{self.namespace}:{key}"
        await self.rc.rpush(key, item)


queue: RedisQueue | None = None


class AsyncRedis(Redis):
    async def execute_command(self, *args, **kwargs):
        return await super().execute_command(*args, **kwargs)


async def connect_redis(host: str, port: int) -> AsyncRedis:
    redis_client = AsyncRedis(host=host, port=port, db=0, decode_responses=True)
    try:
        await redis_client.ping()
    except redis_exceptions.ConnectionError as e:
        logging.error("Was not able to connect to Redis")
        logging.error(e)
        logging.error(traceback.format_exc())

    logging.info(f"Connection established to {(host, port)} Redis")
    return redis_client


redis_cache_connector: Redis | None = None
