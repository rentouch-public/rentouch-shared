from rentouch_shared.auth.token import AppToken
from rentouch_shared.role import Role


def test_values():
    token = AppToken(
        {
            "tenantId": "tid",
            "tenantName": "t",
            "company_id": "cid",
            "sub": "u",
            "roles": ["r"],
        }
    )
    assert token.tenant_id == "tid"
    assert token.tenant_name == "t"
    assert token.user == "u"
    assert token.roles == ["r"]
    assert token.company_id == "cid"
    assert token.authid == "cid.u"


def test_get_roles():
    token = AppToken({})
    assert token.get_role(Role.USER) == "user"

    token = AppToken({"roles": ["admin"]})
    assert token.get_role(Role.USER) == "admin"
