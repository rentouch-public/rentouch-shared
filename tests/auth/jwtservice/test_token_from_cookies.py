import jwt
import pytest

from rentouch_shared.auth.jwtservice import CookiesNotFound
from rentouch_shared.auth.jwtservice import JwtService
from rentouch_shared.auth.jwtservice import RevokedTokenError
from rentouch_shared.auth.token import AppToken
from rentouch_shared.auth.token import Jwt
from rentouch_shared.context import ContextSession

service = JwtService("key", None, ContextSession())


def prepare(mocker, is_revoked):
    mocker.patch.object(JwtService, "_is_revoked", return_value=is_revoked)
    return mocker.patch.object(jwt, "decode", return_value={"sub": "user"})


@pytest.mark.asyncio
async def test_token_from_cookies_no_cookies():
    with pytest.raises(CookiesNotFound):
        await service.token_from_cookies(None, None)


@pytest.mark.asyncio
async def test_token_from_cookies_revoked(mocker):
    prepare(mocker, True)
    with pytest.raises(RevokedTokenError):
        await service.token_from_cookies("access", "client")


@pytest.mark.asyncio
async def test_token_from_cookies(mocker):
    decode = prepare(mocker, False)

    token = await service.token_from_cookies("access", "client")
    assert token == AppToken(decode.return_value)
    decode.assert_called_with("access", "key", audience="client", algorithms=["RS256"])


@pytest.mark.asyncio
async def test_token_from_cookies_custom_key(mocker):
    class MyToken(Jwt):
        def __init__(self, data):
            super().__init__(data)

        def __eq__(self, other):
            return isinstance(other, MyToken) and other.data == self.data

    decode = prepare(mocker, False)

    token = await service.token_from_cookies("access", "client", "my_key", MyToken)
    assert token == MyToken(decode.return_value)
    decode.assert_called_with(
        "access", "my_key", audience="client", algorithms=["RS256"]
    )
