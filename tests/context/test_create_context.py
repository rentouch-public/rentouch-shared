from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext


def test_create_context_with_correlation_id():
    session = ContextSession()
    context = ExecutionContext("company", "user", "corr", session)
    assert context.company == "company"
    assert context.user_id == "user"
    assert context.correlation_id == "corr"
    assert context._session == session


def test_create_context_without_correlation_id():
    session = ContextSession()
    context = ExecutionContext("company", "user", None, session)
    assert context.correlation_id.startswith("X")
    assert len(context.correlation_id) == 36
