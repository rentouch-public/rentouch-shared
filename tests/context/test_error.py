import pytest

from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext


def test_no_error():
    context = ExecutionContext("company", "user", "corr", ContextSession())
    context.raise_creation_error()
    # no exception has been raised


def test_context_with_error(mocker):
    context = ExecutionContext(
        "company",
        "user",
        "corr",
        ContextSession(),
        creation_error=Exception("ups"),
    )
    with pytest.raises(Exception, match="ups"):
        context.raise_creation_error()
