import pytest

from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext


@pytest.mark.asyncio
async def test_publish_message(mocker):
    publish = mocker.patch.object(ContextSession, "publish_message")
    context = ExecutionContext("company", "user", "corr", ContextSession())
    context.publish_message("uri", "2", a=42)
    publish.assert_called_with("uri", "2", a=42, **{"correlation-id": "corr"})


@pytest.mark.asyncio
async def test_call_procedure(mocker):
    call = mocker.patch.object(ContextSession, "call_procedure")
    context = ExecutionContext("company", "user", "corr", ContextSession())
    await context.call_procedure("uri", "2", a=42)
    call.assert_called_with("uri", "2", a=42, **{"correlation-id": "corr"})
