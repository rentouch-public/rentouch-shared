from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext


def test_with_company():
    session = ContextSession()
    context = ExecutionContext(
        company="company", user_id="user", correlation_id="corr", session=session
    )
    new_context = context.with_company("other-company")
    assert new_context.company == "other-company"
    assert new_context.user_id == "user"
    assert new_context.correlation_id == "corr"
    assert new_context._session == session
