from sentry_sdk.hub import Hub

from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext
from rentouch_shared.context import _empty


def test_ok():
    assert ExecutionContext.current() == _empty
    with ExecutionContext("comp", "user", "1234", ContextSession()) as context:
        assert ExecutionContext.current() == context
        assert Hub.current.scope._user == {
            "id": "comp",
            "user-id": "user",
            "company": "comp",
            "correlation-id": "1234",
        }

    assert ExecutionContext.current() == _empty
    assert Hub.current.scope._user is None


def test_exception():
    try:
        with ExecutionContext("comp", "user", "1234", ContextSession()):
            raise Exception("bla")
    except Exception:
        assert ExecutionContext.current() == _empty
        assert Hub.current.scope._user is None
