from twisted.web.http import Request
from twisted.web.http_headers import Headers


class TXRequest(Request):
    def __init__(self, **kwargs):
        self.headers = kwargs.get("headers", {})
        self.cookies = kwargs.get("cookies", {})
        self.responseHeaders = Headers()

    def getHeader(self, name):
        try:
            return self.headers[name]
        except KeyError:
            return None

    def getCookie(self, name):
        try:
            return self.cookies[name]
        except KeyError:
            return None
