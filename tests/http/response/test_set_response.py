from rentouch_shared.http.response import set_response


def test_success_response():
    response_code, headers, data = set_response({"success": True, "value": 42})
    assert data == '{"success": true, "value": 42}'
    assert headers == {
        "Cache-Control": "no-cache",
        "Content-Type": "application/json",
    }
    assert response_code == 200


def test_custom_code_response():
    response_code, headers, data = set_response({"value": 42}, 204)
    assert data == '{"value": 42}'
    assert response_code == 204


def test_fail_response():
    response_code, headers, data = set_response({"value": 42})
    assert data == '{"value": 42}'
    assert response_code == 500


def test_tuple_response():
    response_code, headers, data = set_response((204, {"value": 42}))
    assert data == '{"value": 42}'
    assert response_code == 204
