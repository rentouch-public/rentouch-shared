from rentouch_shared.http.response import success_response


def test_success_response():
    response = success_response({"value": 42})
    assert response == {"success": True, "response": {"value": 42}}
