from rentouch_shared.http.response import response_of_data


def test_response_of_exception():
    response = response_of_data(Exception("ups"))
    assert response == {
        "success": False,
        "error": {"code": "Exception", "message": "ups", "details": None},
    }


def test_response_of_value():
    response = response_of_data("all ok")
    assert response == {"success": True, "response": "all ok"}


def test_response_of_success():
    response = response_of_data({"success": True, "response": "all ok"})
    assert response == {"success": True, "response": "all ok"}


def test_response_of_success_with_data():
    response = response_of_data({"success": True, "data": "all ok"})
    assert response == {"success": True, "response": "all ok"}


def test_response_of_dict():
    response = response_of_data({"data": "all ok"})
    assert response == {"success": True, "response": {"data": "all ok"}}


def test_response_of_empty_fail():
    response = response_of_data({"success": False})
    assert response == {
        "success": False,
        "error": {
            "code": "UNKNOWN_ERROR",
            "message": "An unexpected error occurred",
            "details": None,
        },
    }


def test_response_of_fail():
    response = response_of_data({"success": False, "errorCode": "666", "error": "ups"})
    assert response == {
        "success": False,
        "error": {"code": "666", "message": "ups", "details": None},
    }
