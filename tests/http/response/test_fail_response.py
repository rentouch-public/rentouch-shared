from rentouch_shared.http.response import fail_response


def test_fail_response():
    response = fail_response("666", "ups")
    assert response == {
        "success": False,
        "error": {"code": "666", "message": "ups", "details": None},
    }


def test_fail_response_with_details():
    response = fail_response("666", "ups", "uiui")
    assert response == {
        "success": False,
        "error": {"code": "666", "message": "ups", "details": "uiui"},
    }
