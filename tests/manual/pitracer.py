"""
To be able to run this script, do a:
- venv/bin/python setup.py develop (loosely links this into your env)
"""
import asyncio
import os

from rentouch_shared.context import ContextSession
from rentouch_shared.context import ExecutionContext
from rentouch_shared.pitracer import Tracer
from rentouch_shared.services.rqueue import RedisQueue
from rentouch_shared.services.rqueue import connect_redis

HOST = os.getenv("HOST", default="localhost")
PORT = int(os.getenv("PORT", default="7379"))


async def main():
    ctx = ExecutionContext("company_id", "user_id", "correlation_id", ContextSession())
    rc = await connect_redis(HOST, PORT)
    queue = RedisQueue(rc)
    tracer = Tracer(queue=queue, service_name="rentouch_shared-test")
    await asyncio.sleep(0.8)
    print("start push...")
    for i in range(20):
        data = {"index": i}
        await tracer.log("quartabiz", "test-script", data=data, context=ctx)


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
