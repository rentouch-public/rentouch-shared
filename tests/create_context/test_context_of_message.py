from rentouch_shared.create_context import context_of_message


class Msg:
    def __init__(self, authid, role, procedure, args, kwargs):
        self.caller_authid = authid
        self.caller_authrole = role
        self.procedure = procedure
        self.args = args
        self.kwargs = kwargs


def test_context_of_frontend_message(mocker):
    msg = Msg("company.user", "admin", "", (), {"correlation-id": "1234"})
    context = context_of_message(msg, mocker.sentinel.session)

    assert context.company == "company"
    assert context.user_id == "user"
    assert context.correlation_id == "1234"
    assert context._session == mocker.sentinel.session
    assert "correlation-id" not in msg.kwargs


def test_context_of_backend_message(mocker):
    msg = Msg(
        "backend", "admin", "ch.rentouch.piplanning.6.room.company.endpoint", (), {}
    )
    context = context_of_message(msg, mocker.sentinel.session)

    assert context.company == "company"
    assert context.user_id == "backend"
    assert context.correlation_id.startswith("X")
    assert context._session == mocker.sentinel.session
    assert "correlation-id" not in msg.kwargs


def test_context_of_auth_call(mocker):
    msg = Msg(
        "backend", "admin", None, [{"authid": "company.user", "authrole": "role"}], {}
    )
    context = context_of_message(msg, mocker.sentinel.session)

    assert context.company == "company"
    assert context.user_id == "user"
    assert context.correlation_id.startswith("X")
    assert context._session == mocker.sentinel.session
    assert "correlation-id" not in msg.kwargs


def test_context_of_unknown(mocker):
    msg = Msg("backend", "admin", None, [], {})
    context = context_of_message(msg, mocker.sentinel.session)

    assert context.company == "<unknown>"
    assert context.user_id == "<unknown>"
    assert context.correlation_id.startswith("X")
    assert context._session == mocker.sentinel.session
    assert "correlation-id" not in msg.kwargs
