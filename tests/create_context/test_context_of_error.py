from rentouch_shared.context import ContextSession
from rentouch_shared.create_context import context_of_error


def test_context_of_error():
    session = ContextSession()
    context = context_of_error(Exception("ups"), "1234", session)

    assert context.company == "<unknown>"
    assert context.user_id == "<unknown>"
    assert context.correlation_id == "1234"
    assert context._session == session
    assert str(context.creation_error) == "ups"
