from rentouch_shared.auth.token import AppToken
from rentouch_shared.create_context import context_of_token


def test_context_of_token():
    token = AppToken({"company_id": "company", "sub": "user", "roles": ["admin"]})
    context = context_of_token(token, "1234", {})

    assert context.company == "company"
    assert context.user_id == "user"
    assert context.correlation_id == "1234"
    assert context._session == {}
