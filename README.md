# rentouch-shared module

Common functionality and data to use in pi microservices.

# How to install and run for local development

## Installation

Install a virtualenenv which holds all the needed modules.

```bash
virtualenv venv -p python3
venv/bin/pip install -U pip
venv/bin/pip install -r requirements.txt -r requirements_dev.txt
```
## Pre-commit hooks

The type checking step `pyright` expects the pip modules to be located either in your environment
or in the `venv` directory at the project level.

1. Install by
    - `venv/bin/pip install pre-commit`
    - `venv/bin/pre-commit install`
2. Run manually by: `pre-commit run --all-files`


## pitracer
For auditing purpose all actions should be logged via pitracer. There are
multiple ways to log to pitracer using the pitracer module which is part of
this rentouch-shared module.
